<?php //관리자님께 ?>
<!doctype html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <title><?php echo $config['cf_title']; ?> - <?php echo $order_case ?> 알림 메일</title>
</head>

<?php
$cont_st = 'margin:0 auto 20px;width:94%;border:0;border-collapse:collapse';
$caption_st = 'padding:0 0 5px;font-weight:bold';
$th_st = 'padding:5px;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;background:#f5f6fa;text-align:left';
$td_st = 'padding:5px;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9';
$empty_st = 'padding:30px;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;text-align:center';
$ft_a_st = 'display:block;padding:30px 0;background:#484848;color:#fff;text-align:center;text-decoration:none';
?>

<body>
<div style="margin:30px auto;width:600px;border:10px solid #f7f7f7">
    <div style="border:1px solid #dedede">
        <h1 style="margin:0 0 20px;padding:30px 30px 20px;background:#f7f7f7;color:#555;font-size:1.4em">
            <?php echo $config['cf_title'];?> - <?php echo $order_case ?> 주문이 접수되었습니다.
        </h1>

        <p style="<?php echo $cont_st; ?>">
            <strong>주문번호 <?php echo $od_id; ?></strong><br>
            본 메일은 <?php echo G5_TIME_YMDHIS; ?> (<?php echo get_yoil(G5_TIME_YMDHIS); ?>)을 기준으로 작성되었습니다.
        </p>

        <a href="<?php echo G5_ADMIN_URL.'/shop_admin/orderform.php?od_id='.$od_id; ?>" target="_blank" style="<?php echo $ft_a_st; ?>">관리자 모드에서 주문 확인</a>

    </div>
</div>
</body>
</html>
